﻿using Common.Sdk.Logging;
using Microsoft.AspNetCore.Mvc;

namespace Domain.QuoteManager.API.Controllers
{
    [Route("api/[controller]", Name = "ConfigSets", Order = 1)]
    [ApiController]
    public class ConfigSetsProcessController : ControllerBase
    {
        private readonly ILogger _logger;
        public ConfigSetsProcessController(ILogger logger)
        {
            _logger = logger;
        }

        // GET api/<ConfigSetsProcessController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "SOME_STRING";
        }

        // POST api/<ConfigSetsProcessController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<ConfigSetsProcessController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<ConfigSetsProcessController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
