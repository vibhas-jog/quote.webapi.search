﻿using Common.Sdk.Logging;
using Microsoft.AspNetCore.Mvc;

namespace Domain.QuoteManager.API.Controllers
{
    [Route("api/[controller]", Name = "ConfigLineItems", Order = 1)]
    [ApiController]
    public class ConfigSetsExperienceController : ControllerBase
    {
        private readonly ILogger _logger;
        public ConfigSetsExperienceController(ILogger logger)
        {
            _logger = logger;
        }

        // GET api/<ConfigSetsExperienceController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "SOME_STRING";
        }

        // POST api/<ConfigSetsExperienceController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<ConfigSetsExperienceController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<ConfigSetsExperienceController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
