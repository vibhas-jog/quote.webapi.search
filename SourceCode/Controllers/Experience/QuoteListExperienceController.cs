﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Sdk.Logging;
using Quote.WebApi.Search.BusinessLogic.Entities;
using Quote.WebApi.Search.BusinessLogic.Interface;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Domain.QuoteManager.API.Controllers.Experience
{
    [Route("api/[controller]/V{ver:apiVersion}")]
    [ApiController]
    public class QuoteListExperienceController : ControllerBase
    {
        private readonly ILogger _logger;
        private IQuoteListLogic _quoteListLogic;
        

        public QuoteListExperienceController(ILogger logger, IQuoteListLogic quoteListLogic)
        {
            _logger = logger;
            _quoteListLogic = quoteListLogic;
        }
       

        [HttpGet]
        [Route("GetQuotes")]
        public async Task<QuoteListResponse> Get(string customernumber, string isocountrycode, int pageIndex, int recordperPage, string Sorting, string SortingColumnName)
        {
            var ViewModel = await _quoteListLogic.GetQuoteList(customernumber, isocountrycode, pageIndex, recordperPage, Sorting, SortingColumnName);

            return ViewModel;

        }


        // POST api/<QuoteListExperienceController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<QuoteListExperienceController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<QuoteListExperienceController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
