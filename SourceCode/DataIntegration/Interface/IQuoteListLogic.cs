﻿using Quote.WebApi.Search.DataIntegration.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.DataIntegration.Interface
{
    public interface IQuoteListLogic
    {
        Task<RetrieveQuoteAndSearchResponse> GetQuoteList(Executequoteandsearchrequest quoteRequest);
    }
}
