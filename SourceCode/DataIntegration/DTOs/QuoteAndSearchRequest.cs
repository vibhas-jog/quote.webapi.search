﻿using Domain.QuoteManager.API.BusinessLogic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quote.WebApi.Search.DataIntegration.DTOs
{
    public class QuoteAndSearchRequest
    {
        public Requestpreamble requestpreamble { get; set; }
        public RetrieveQuoteSearchRequest retrievequoterequest { get; set; }
    }
    public class Executequoteandsearchrequest
    {
        public QuoteAndSearchRequest quotesearchrequest { get; set; }
    }
}
