﻿using Domain.QuoteManager.API.BusinessLogic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.DataIntegration.DTOs
{
    public class QuoteAndSearchRequest
    {
        public Requestpreamble requestpreamble { get; set; }
        public RetrieveQuoteSearchRequest retrievequoterequest { get; set; }
    }
    public class Executequoteandsearchrequest
    {
        public QuoteAndSearchRequest quotesearchrequest { get; set; }
    }
}
