﻿
using Quote.WebApi.Search.BusinessLogic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quote.WebApi.Search.BusinessLogic.Interface
{
    public interface IQuoteListLogic
    {
        //Task<RetrieveQuoteAndSearchResponse> GetQuoteList(Executequoteandsearchrequest quoteRequest);
        Task<QuoteListResponse> GetQuoteList(string customernumber, string isocountrycode, int pageIndex, int recordperPage, string Sorting, string SortingColumnName);
        
    }
}
