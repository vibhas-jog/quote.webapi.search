﻿using Quote.WebApi.Search.BusinessLogic.Entities;
using Quote.WebApi.Search.DataIntegration.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuoteListResponse = Quote.WebApi.Search.BusinessLogic.Entities.QuoteListResponse;

namespace Quote.WebApi.Search.BusinessLogic.Interface
{
    public interface IQuoteSearchSystemToExpApiMapper
    {
        public QuoteListResponse MapToViewModel(RetrieveQuoteAndSearchResponse source);
    }
}
