﻿
using Domain.QuoteManager.API.BusinessLogic.Entities;
using Domain.QuoteManager.API.DataIntegration.Interface;
using Quote.WebApi.Search.BusinessLogic.Entities;
using Quote.WebApi.Search.BusinessLogic.Interface;
using Quote.WebApi.Search.DataIntegration.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuoteListResponse = Quote.WebApi.Search.BusinessLogic.Entities.QuoteListResponse;

namespace Quote.WebApi.Search.BusinessLogic
{
    public class QuoteListLogic : IQuoteListLogic
    {
        private IQuoteListService _quoteListService;
        private IQuoteSearchSystemToExpApiMapper _quoteSearchSystemToExpApiMapper;

        public QuoteListLogic(IQuoteListService quoteListService, IQuoteSearchSystemToExpApiMapper quoteSearchSystemToExpApiMapper)
        {
            _quoteListService = quoteListService;
            _quoteSearchSystemToExpApiMapper = quoteSearchSystemToExpApiMapper;

        }

        public async Task<QuoteListResponse> GetQuoteList(string customernumber, string isocountrycode, int pageIndex, int recordperPage, string Sorting, string SortingColumnName)
        {
            Requestpreamble requestpreamble = new Requestpreamble()
            {
                customernumber = customernumber,
                isocountrycode = isocountrycode
            };
            RetrieveQuoteSearchRequest search = new RetrieveQuoteSearchRequest()
            {
                PageIndex = pageIndex,
                RecordPerPage = recordperPage,
                Sorting = Sorting,
                SortingColumnName = SortingColumnName

            };

            QuoteAndSearchRequest quotesearch = new QuoteAndSearchRequest()
            {
                requestpreamble = requestpreamble,
                retrievequoterequest = search
            };


            Executequoteandsearchrequest extquoteReq = new Executequoteandsearchrequest()
            {
                quotesearchrequest = quotesearch

            };
            RetrieveQuoteAndSearchResponse retrieveQuoteAndSearchResponse = new RetrieveQuoteAndSearchResponse();
            retrieveQuoteAndSearchResponse =  await _quoteListService.GetQuoteList(extquoteReq);
            var ViewModel = _quoteSearchSystemToExpApiMapper.MapToViewModel(retrieveQuoteAndSearchResponse);
            return ViewModel;
        }
    }
}
