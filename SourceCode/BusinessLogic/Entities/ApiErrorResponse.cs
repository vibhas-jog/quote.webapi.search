﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.BusinessLogic.Entities
{
    public class ApiErrorResponse : ApiResponse
    {
        public ApiErrorResponse(int statusCode, string message = null) : base(statusCode, message)
        {

        }

        public IEnumerable<string> Errors { get; set; }
    }
}
