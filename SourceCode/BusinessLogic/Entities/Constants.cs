﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.BusinessLogic.Entities.Constants
{
    public struct Status
    {
        public const string Passed = "Passed";
        public const string Failed = "Failed";

    }

    public struct StatusCode
    {
        public const string TwoHundred = "200";
        public const string FourHundred = "400";
        public const string FiveHundred = "500";
    }


    public struct QuoteProductAction
    {
        public const string Added = "Added";
        public const string Modified = "Modified";
        public const string Deleted = "Deleted";
        public const string Unchanged = "Unchanged";
    }

    public struct StatusMessage
    {
        public const string Success = "Success";
        public const string InvalidRequest = "Invalid Request";
        public const string RetrieveErrorFound = "Error in Retrieve";
        public const string HeaderMandatoryFieldsMissing = "Header Mandatory Fields Missing";
        public const string MandatoryFieldsMissing = "Mandatory Fields Missing";
        public const string NotFound = "Notfound";

        public const string RecordsFound = "Records Found";
        public const string RecordsNotFound = "Records Not Found";
        public const string MultipleRecordsFound = "Multiple Records Found";

        public const string CustNotFound = "Customer Not Found";
        public const string CustMultipleRecordsFound = "Multiple Customer Records Found";

        public const string CustContactNotFound = "Customer Contact Not Found";
        public const string CustContactAlreadyExists = "Customer Contact Already Exists";
        public const string CustContactEmailshhouldbesame = "Customer Contact & Email should be same";

        public const string CustContactMultipleRecordsFound = "Multiple Customer Contact Records Found";

        public const string OppGuidNotCorrect = "Opportunity Guid is not Correct";

        public const string QuoteNotInActiveOrDraftStatus = "Quote can only be closed from Draft/Active State";
        public const string QuoteNotInDraftStatus = "Quote can only be activated from Draft State";

        public const string QuoteNotInDraftOrActiveStatus = "Only Draft/Active Quote can be converted to Order";

        public const string TeamNotFound = "Team Not Found";
        public const string UserNotFound = "User Not Found";
        public const string VendorNotFound = "Vendor Not Found";

    }
    public struct CSVResourceKeys
    {
        // CSV
        public const string account = "account";
        public const string address = "address";
        public const string contact = "contact";
        public const string contactleasing = "contactleasing";
        public const string dart = "dart";
        public const string dartexpirationdate = "dartexpirationdate";
        public const string dartinfo = "dartinfo";
        public const string datecreated = "datecreated";
        public const string dealid = "dealid";
        public const string discountofflist = "discountofflist";
        public const string email = "email";
        public const string quoteName = "quotename";
        public const string estimateid = "estimateid";
        public const string euinfo = "euinfo";
        public const string extendedresellerprice1 = "extendedresellerprice1";
        public const string extendedresellerprice2 = "extendedresellerprice2";
        public const string extenededlistprice = "extenededlistprice";
        public const string imsku = "imsku";
        public const string ingrammicro = "ingrammicro";
        public const string ingrammicroeveryeffort = "ingrammicroeveryeffort";
        public const string learnmore = "learnmore";
        public const string leasepricing = "leasepricing";
        public const string leasinginfo = "leasinginfo";
        public const string line = "line";
        public const string name1 = "name1";
        public const string name2 = "name2";
        public const string partnergo = "partnergo";
        public const string phone1 = "phone1";
        public const string phone2 = "phone2";
        public const string productdescription = "productdescription";
        public const string qty = "qty";
        public const string quoteid1 = "quoteid1";
        public const string quoteid2 = "quoteid2";
        public const string quoteinfo = "quoteinfo";
        public const string resellerinfo = "resellerinfo";
        public const string status = "status";
        public const string termsmonth = "termsmonth";
        public const string unitpricelist = "unitpricelist";
        public const string unitresellerprice = "unitresellerprice";
        public const string version = "version";
        public const string visitpartnergo = "visitpartnergo";
        public const string vpn = "vpn";
        public const string wevalueyour = "wevalueyour";
        public const string winmoreopp = "winmoreopp";
        public const string isreadytoorder = "isreadytoorder";
        public const string quotebasedondartid = "quotebasedondartid";
        public const string ccwrquoteid = "ccwrquoteid";
        public const string endusertype = "endusertype";
        public const string quoteexpirationdate = "quoteexpirationdate";
    }
    public struct XLSXResourceKeys
    {
        // XLSX

        public const string account = "account";
        public const string address = "address";
        public const string contact = "contact";
        public const string contactleasing = "contactleasing";
        public const string dart = "dart";
        public const string dartexpirationdate = "dartexpirationdate";
        public const string dartinfo = "dartinfo";
        public const string datecreated = "datecreated";
        public const string dealid = "dealid";
        public const string discountofflist = "discountofflist";
        public const string email = "email";
        public const string quoteName = "quotename";
        public const string estimateid = "estimateid";
        public const string euinfo = "euinfo";
        public const string extendedlistprice = "extendedlistprice";
        public const string extendedresellerprice1 = "extendedresellerprice1";
        public const string extendedresellerprice2 = "extendedresellerprice2";
        public const string imsku = "imsku";
        public const string ingrammicro = "ingrammicro";
        public const string ingrammicromakeeveryeffort = "ingrammicromakeeveryeffort";
        public const string learnmore = "learnmore";
        public const string leaseprice = "leaseprice";
        public const string leasinginfo = "leasinginfo";
        public const string line = "line";
        public const string name1 = "name1";
        public const string name2 = "name2";
        public const string partnergo = "partnergo";
        public const string phone1 = "phone1";
        public const string phone2 = "phone2";
        public const string productdescription = "productdescription";
        public const string qty = "qty";
        public const string quotebasedondartid = "quotebasedondartid";
        public const string quoteid1 = "quoteid1";
        public const string quoteid2 = "quoteid2";
        public const string quoteinfo = "quoteinfo";
        public const string resellerinfo = "resellerinfo";
        public const string status = "status";
        public const string termmonth = "termmonth";
        public const string thankyou = "thankyou";
        public const string unitlistprice = "unitlistprice";
        public const string unitresellerprice = "unitresellerprice";
        public const string version = "version";
        public const string visitpartnergo = "visitpartnergo";
        public const string vpn = "vpn";
        public const string winmoreopp = "winmoreopp";
        public const string viewquote = "viewquote";
        public const string readytoorder = "readytoorder";
        public const string won = "won";
        public const string closed = "closed";
        public const string ccwrquoteid = "ccwrquoteid";
        public const string endusertype = "endusertype";
        public const string quoteexpirationdate = "quoteexpirationdate";
    }
    public struct Resources
    {
        public const string QuoteCreateEmailExcel = "IM.BusinessServices.Quote.Common.Resources.QuoteCreateEmailExcel";
        public const string QuoteCreateEmailCSV = "IM.BusinessServices.Quote.Common.Resources.QuoteCreateEmailCSV";
    }

    public struct QuoteTypeDisplay
    {
        public const string Incumbent = "Renewal Incumbent";
        public const string Takeover = "Renewal Takeover";
        public const string Education = "Education";
        public const string Renewal = "Renewal";
        public const string EDU = "EDU";


    }

    public struct QuoteType
    {
        public const string Incumbent = "Incumbent";
        public const string Takeover = "Takeover";
        public const string Education = "Education";
        public const string Renewal = "Renewal";
    }

    public struct ResponseMessages
    {
        public const string ResponseStatus_Success = "Success";
        public const string ResponseStatus_Passed = "Passed";
        public const string ResponseStatus_Failed = "Failed";
        public const string ResponseStatus_Error = "Error";
        public const string ResponseStatus_NotFound = "Notfound";
        public const string ResponseStatus_InvalidRequest = "InvalidRequest";


        public const string ResponseCode_Passed = "200";
        public const string ResponseCode_Failed = "400";


        public const string ResponseMsg_RetrieveErrorFound = "Error in Retrieve";
        public const string ResponseMsg_HeaderMandatoryFieldsMissing = "Header Mandatory Fields Missing";

        public const string ResponseMsg_MandatoryFieldsMissing = "Mandatory Fields Missing";


        public const string ResponseMsg_MultipleRecordsFound = "Multiple Records Found";
        public const string ResponseMsg_RecordsNotFound = "Records Not Found";

        public const string ResponseMsg_CustNotFound = "Customer Not Found";
        public const string ResponseMsg_CustMultipleRecordsFound = "Multiple Customer Records Found";

        public const string ResponseMsg_CustContactNotFound = "Customer Contact Not Found";
        public const string ResponseMsg_CustContactMultipleRecordsFound = "Multiple Customer Contact Records Found";
        public const string Response_QuoteNotActivated = "Quote is not active";
    }

    public struct constantMessages
    {
        public const string UniversalDateFormatForJSONRequest = "yyyy-MM-dd";//"yyyy-MM-ddTHH:mm:ssZ";
        public const string DateFormatForJSONRequest = "yyyy-MM-ddTHH:mm:ssZ";
        public const string Request_OpportunityCreate = "CREATE";
        public const string Request_CiscoPortalRequest = "Cisco Portal Request";
        public const string Request_TrueFlag = "Y";
        public const string Request_FalseFlag = "N";
        public const string Request_Costingram = "costingram";
        public const string Request_Costingram1 = "costingram1";

        public const string Request_ProductsNotMatched = "No Matching Products";
        public const string Request_NoDraftQuotes = "No Draft Quotes Found";
        public const string Request_NoProductsAssociatedwithQuote = "No Products associated with Quote Number:";


        public const string emptyBlock = "{}";
        public const string lineBreak = "\r\n";
        //public const string json = "{\"file\":\"IMproductSearch\",\"params\":{\"pageStart\": 0,\"pageSize\": 50,\"listOfProducts\":[\"PB0047\", \"PB0048\"]}}";
        public const string jsonSearchStartString = "{\"id\":\"IMproductSearch_CRM\",\"params\":{\"pageStart\": 0,\"pageSize\": 50,\"listOfProducts\":[";
        public const string jsonSearchEndString = "]}}";
        public const string AppendAccessToken = ":";
        public const string jsonStart = @"\""";
        public const string jsonEnd = @"\""";
        public const string jsonAddComma = ",";
        public const string jsonNextSku = @" \""";
        public const string Status = "Draft";
        public const string RequireContactEmail = "N";

        public const string ACOPDetails = "ACOPDetails";
        public const string ACOPAmount = "ACOPAmount";
        public const string ACOPDescription = "ACOPDescription";
        public const string ACOPExpiryDate = "ACOPExpiryDate";
        public const string ACOPtype = "ACOPtype";

        public const string en_US = "EN-US";
        public const string en_FR = "EN-FR";
        public const string en_BR = "EN-BR";
        public const string en_MX = "EN-MX";

    }
}
