﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Domain.QuoteManager.API.BusinessLogic.Entities
{
    public class Requestpreamble
    {
        [Required(ErrorMessage = "Customer Number can't be blank.")]
        public string customernumber { get; set; }
        public string customercontact { get; set; }
        [Required(ErrorMessage = "Country Code can't be blank.")]
        public string isocountrycode { get; set; }
        public string correlationid { get; set; }
        public bool skipcontactvalidation { get; set; } = true;

    }
}
