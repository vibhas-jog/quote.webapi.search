﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.BusinessLogic.Entities
{
    public class Responsepreamble
    {
        public string responsestatus { get; set; }
        public string statuscode { get; set; }
        public string responsemessage { get; set; }

    }
}
