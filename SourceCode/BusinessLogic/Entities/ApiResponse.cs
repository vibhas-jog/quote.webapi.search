﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.QuoteManager.API.BusinessLogic.Entities
{
    public class ApiResponse
    {
        public ApiResponse(int statusCode, string message = null)
        {
            Message = message ?? GetDefaultMessageForStatusCode(statusCode);
            StatusCode = statusCode;
        }

        public string Message { get; set; }
        public int StatusCode { get; set; }

        private string GetDefaultMessageForStatusCode(int statusCode)
        {
            switch (statusCode)
            {
                case 400: return "A Bad Request";
                case 401: return "Unauthorized, no access to resource";
                case 404: return "Resource not found";
                case 500: return "Internal Server error";
                default: return null;
            }
        }
    }
}
