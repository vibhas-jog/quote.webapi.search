﻿using Quote.WebApi.Search.BusinessLogic.Entities;
using Quote.WebApi.Search.BusinessLogic.Interface;
using Quote.WebApi.Search.DataIntegration.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuoteList = Quote.WebApi.Search.BusinessLogic.Entities.QuoteList;
using QuoteListResponse = Quote.WebApi.Search.BusinessLogic.Entities.QuoteListResponse;

namespace Quote.WebApi.Search.BusinessLogic.Mappers
{
    public class QuoteSearchSystemToExpApiMapper: IQuoteSearchSystemToExpApiMapper
    {
        string[] RenewalSources = { "2000000", "1000000", "100000002" };

        public QuoteListResponse MapToViewModel(RetrieveQuoteAndSearchResponse source)
        {
            if (source == null) return null;
            var viewModel = new List<QuoteList>();
            foreach (var quote in source.quotelist)
            {
                QuoteList q = new QuoteList();

                q.QuoteGuid = quote.QuoteGuid;
                q.QuoteName = quote.QuoteName;
                q.QuoteNumber = quote.QuoteNumber;

                if (quote.Source == "1000004")
                {
                    q.CreatedBy = quote.Contact;
                }
                else if (RenewalSources.Contains(quote.Source) && quote.quotetype == "Renewal")
                {
                    q.CreatedBy = "Renewal";
                }
                else
                {
                    q.CreatedBy = "Ingram Micro";
                }


                q.CreatedOn = quote.Created;
                q.EndUserName = quote.EndUserName;
                q.Modified = quote.Modified;
                q.Status = quote.Status == "Active" ? "Ready to Order" : quote.Status;
                q.QuotePrice = quote.TotalAmount;
                q.quoteexpirydate = quote.EffectiveTo;
                q.RevisionNumber = quote.RevisionNumber;
                q.QuoteNumber_rev = (quote.RevisionNumber != "0") ? quote.QuoteNumber + "_" + quote.RevisionNumber : quote.QuoteNumber;
                viewModel.Add(q);
            }

            QuoteListResponse quotelistResponse = new()
            {
            Quotes = viewModel,
                TotalCount = source.TotalCount
            };

           return quotelistResponse;

        }

    }
}
